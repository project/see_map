
A CCK field that allows you to add "See Map" links to content types that
represent addresses. This field also depends on the Location module, which
provides the API to generate the list of See Map links.


Installation & Configuration
----------------------------

Install both the See Map and Location modules as usual.

First configure the Location module to show the appropriate "See map" links:
- go to Administer / Site configuration / Location
- on the Main settings tab select the desired checkboxes under
  "Enable all available features for locations from the following countries"
- on the "Map links" tab select the desired mapping sites for each country
  (see next section for details on Map Links support)

Second, create the content type that has a physical address (if you have not
done that yet). Add the fields for Street, City, Province, Country and Postal
code. The City, Province and Country are optional (maybe all your addresses
are for a known city so no need to add a field), if you don't have fields
for them make sure you add defualt values in the next step. Postal code is
optional as well, but it does not have a default value.

Third, you will have to add a "See Map" field to your content type:
- go to Administer / Content management / Content types
- click "edit" for the appropriate content type
- click "Add field"
- enter field name, for example "See Map"
- select the "'See Map' links" radio button
- click "Create field"
- under "Widget settings" click "Default value" and select or unselect the
  "Show 'See Map' links" checkbox, you probably want to have this selected
- under "Data settings" enter field names and default values for all the text
  fields
  - you can see the field names when you edit the content type under "Manage
  fields", in the Name column
  - Country and Province values in general are two letter codes, depending on
  the mapping site
  - the default values are used if either you don't specify a corresponding
  field name or if at runtime the value is empty
  - for Trim Unit Number see below
  
When adding nodes of this content type the See Map will have a very simple
widget, just a checkbox to show or hide the "See Map" links. Some nodes will
either not have an address or the address may not be properly mapped by the
mapping site, in which cases you don't want to show the link(s).


Triming Unit Number from Street Address
---------------------------------------

If the street address has a unit number at the beginning then mapping site will
get confused and will not show the proper location. The unit number should be
trimmed in this case.

In order to trim unit numbers you need to enter a regular expression that will
match a optional unit number in your street address. Use the "Trim Unit Number"
field of the "See Map" configuration.

For example, all these street addresses:
#101 123 Main Street
#101-123 Main Street
#101 - 123 Main Street
101 - 123 Main Street
should be trimmed to:
123 Main Street

This street address should not be trimmed:
101 123 Main Street

In this last case the unit number is ambiguous, if the street name is a number
then you can have two numbers at the beginning of the street address without
a unit number:
123 54 Avenue

In the above example 123 is the street number (number of house on that street)
and 54 is the street name.

In order to be safe consider a unit number only if it has # in front or if it
is separated with a dash by the rest of the address.

Here is a regular expression that does that:
/^#\w+\s*-?\s*|\w+\s*-\s*/

See the documentation for PCRE regex pattern syntax:
http://php.net/manual/en/reference.pcre.pattern.syntax.php


Map Links Support in Location Module
------------------------------------

The Location module needs to have Map Links support for your country for all
this to work. Currently only three countries have this support: Canada,
Germany and US.

In the location module, in the 'supported' sub-folder, check the location.??.inc
files. Look at the location_map_link_* functions in the location.us.inc file,
it is quite easy to add support for your own country if you need.

If Yahoo, Google or MapQuest support your country then it is just a mater of
copy and paste and slight editing of the existing functions. Make sure you
submit your changes as a patch to the Location module.
